import UIKit

class ViewController: UIViewController {
    
    // Store the numbers in form of strings
    var numberOne: String = ""
    var numberTwo: String = ""
    
    // Store the numbers in form of Double
    var firstNum = 0.0
    var secondNum = 0.0
    
    // Store the sign value and check no of times sign key is pressed
    var sign : Character = "A"
    var signCount = 0
    
    // Store the answer
    var answer = 0.0
    
    // Temporary variables to store numbers
    var num = 0.0
    var negCount = 0.0
    var decimalCount = 0
    
    @IBOutlet weak var resultDisplay: UILabel!
    
    @IBOutlet weak var signDisplay: UILabel!
    
    // Append function creates the numbers for calculations
    func append (temp : Character){
        
        if signCount == 1 {
            
            numberTwo.append(temp)
            resultDisplay.text = numberTwo
            
        }else {
            
            numberOne.append(temp)
            resultDisplay.text = numberOne
        }
    }
    // decimal added to number
    @IBAction func decimal(sender: AnyObject) {
        
        var temp : Character = "."
        
       decimalCount++
        
        if decimalCount == 1 {
            
        append(temp)
            
        }
    }
    // zero added to number
    @IBAction func zero(sender: AnyObject) {
        
        var temp : Character = "0"
        
        append(temp)
    }
    // 1 added to number
    @IBAction func one(sender: AnyObject) {
        
        var temp : Character = "1"
        
        append(temp)
        
    }
    // 2 added to number
    @IBAction func two(sender: AnyObject) {
        
        var temp : Character = "2"
        
        append(temp)
    }
    // 3 added to number
    @IBAction func three(sender: AnyObject) {
        
        var temp : Character = "3"
        
        append(temp)
    }
    // 4 added to number
    @IBAction func four(sender: AnyObject) {
        
        var temp : Character = "4"
        
        append(temp)
    }
    // 5 added to number
    @IBAction func five(sender: AnyObject) {
        
        var temp : Character = "5"
        
        append(temp)
    }
    // 6 added to number
    @IBAction func six(sender: AnyObject) {
        
        var temp : Character = "6"
        
        append(temp)
    }
    // 7 added to number
    @IBAction func seven(sender: AnyObject) {
        
        var temp : Character = "7"
        
        append(temp)
    }
    // 8 added to number
    @IBAction func eight(sender: AnyObject) {
        
        var temp : Character = "8"
        
        append(temp)
    }
    // 9 added to number
    @IBAction func nine(sender: AnyObject) {
        
        var temp : Character = "9"
        
        append(temp)
        
    }
    // function to negate the number
    @IBAction func negate(sender: AnyObject) {
        
        
            if answer != 0.0 {
                
                firstNum = answer
                
            }else {
                
                firstNum = numberOne.toDouble()!
            }

            firstNum = firstNum * pow(-1.0, ++negCount)
            resultDisplay.text = firstNum.description
            num = firstNum
    }
    // create the first number when any sign key is pressed
    func buildFirstNUMBER () {
        
        firstNum = numberOne.toDouble()!
        num = firstNum
        signCount = 1
        decimalCount = 0
    }
    
    // addition of two numbers
    @IBAction func addition(sender: AnyObject) {
        
        buildFirstNUMBER()
        sign = "+"
        signDisplay.text = "+"
        
    }
    // Subtraction of two numbers
    @IBAction func subtraction(sender: AnyObject) {
        
        buildFirstNUMBER()
        sign = "-"
        signDisplay.text = "-"
    }
    // division of two numbers
    @IBAction func division(sender: AnyObject) {
        
        buildFirstNUMBER()
        sign = "/"
        signDisplay.text = "/"
        
    }
    // multiplication of two numbers
    @IBAction func multiplication(sender: AnyObject) {
        
        buildFirstNUMBER()
        sign = "X"
        signDisplay.text = "X"
    }
   // remove the last character or the sign
    @IBAction func cancel(sender: AnyObject) {
        
        if signCount == 1 {
            
            firstNum = num
            secondNum = 0.0
            signDisplay.text = ""
            
        }else {
            
            if signCount == 1 {
                
                numberTwo = dropLast(numberTwo)
                resultDisplay.text = numberTwo
                
            }else {
                
                numberOne = dropLast(numberOne)
                resultDisplay.text = numberOne
            }
        }
    }
    // clear everything
    @IBAction func clearAll(sender: AnyObject) {
        
        numberOne = ""
        numberTwo = ""
        firstNum = 0.0
        secondNum = 0.0
        sign = "A"
        signCount = 0
        answer = 0.0
        num = 0.0
        resultDisplay.text = ""
        signDisplay.text = ""
        negCount = 0.0
        
    }
    // calculate the result of calculation and display everything
    @IBAction func result(sender: AnyObject) {
       
        secondNum = numberTwo.toDouble()!
        if answer != 0.0 {
            
            firstNum = answer
            
        }else {
            
            firstNum = num * pow(-1.0, negCount)
        }
        
        if sign == "+" {
            
            answer = secondNum + firstNum
            resultDisplay.text = answer.description
            
        }else if sign == "-" {
            
            answer = firstNum - secondNum
            resultDisplay.text = answer.description
            
        }else if sign == "X" {
            
            answer = secondNum * firstNum
            resultDisplay.text = answer.description
            
        }else if sign == "/" {
            
            if secondNum == 0 {
                
                resultDisplay.text = "Divide by 0 exception"
                signDisplay.text = ""
                
            }else {
                
                answer = firstNum / secondNum
                resultDisplay.text = answer.description
            }
        }
        
        numberOne = ""
        numberTwo = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
// extension created for converting string to double
extension String {
    func toDouble() -> Double? {
        var formatter = NSNumberFormatter()
        if let number = formatter.numberFromString(self) {
            return number.doubleValue
        }
        return nil
    }
}